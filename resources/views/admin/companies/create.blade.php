@extends('admin.layout', ['page_title' => 'Company', 'page_description' => 'Create a new company'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{ Form::open(['route' => 'companies.store', 'files' => true]) }}
                        @include('admin/companies/form_fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection