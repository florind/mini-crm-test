@extends('admin.layout', ['page_title' => $company->name, 'page_description' => 'Edit details'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{ Form::model($company, ['route' => ['companies.update', 'company' => $company->id] , 'files' => true]) }}
                        @include('admin/companies/form_fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection