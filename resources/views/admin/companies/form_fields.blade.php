<div class="form-group required {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('name', 'Company Name') }}
    {{ Form::text('name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {{ Form::label('email', 'Company Email') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
    {{ Form::label('website', 'Company Website') }}
    {{ Form::text('website', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
    {{ Form::label('logo', 'Company Logo') }}
    {{ Form::file('logo', ['accept' => 'image/*']) }}

    <p class="help-block">Must be at least 100x100 px.</p>
</div>

<div class="box-footer">
    {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
</div>