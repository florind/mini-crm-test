@extends('admin.layout', ['page_title' => 'Companies', 'page_description' => 'Listing'])

@section('actions')

@endsection

@section('content')
    {{ $companies->links() }}
    <div class="box box-primary">
        <div class="box-header with-border">
            <a href="{{ route('companies.create') }}" class="btn btn-success">create company</a>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped table-condensed datatable">
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Website</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @if($companies->count() === 0)
                    <tr>
                        <td colspan="5" align="center"><span class="text-muted">No companies</span></td>
                    </tr>
                @endif
                @foreach($companies as $company)
                    <tr>
                        <td valign="middle">
                            @php
                                $logo = null === $company->logo ? "https://via.placeholder.com/100x80/FAFAFA/E4E4E4?text=logo" : Storage::url($company->logo);
                            @endphp
                            <img src="{{ $logo }}" alt="" class="company-logo">
                        </td>
                        <td valign="middle">{{ $company->name ?? "-" }}</td>
                        <td valign="middle">{{ $company->email ?? "-" }}</td>
                        <td valign="middle">{{ $company->website ?? "-" }}</td>
                        <td valign="middle">
                            <div class="btn-group" role="group">
                                <a class="btn btn-default" href="{{ route('companies.view', ['company' => $company->id]) }}" role="button">
                                    <i class="fa fa-fw fa-eye"></i>
                                </a>
                                <a class="btn btn-default" href="{{ route('companies.edit', ['company' => $company->id]) }}" role="button">
                                    <i class="fa fa-fw fa-pencil"></i>
                                </a>
                                <a class="btn btn-default action_list_delete" data-token="{{ @csrf_token() }}" href="{{ route('companies.delete', ['company' => $company->id]) }}" role="button">
                                    <i class="fa fa-fw fa-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    {{ $companies->links() }}

@endsection