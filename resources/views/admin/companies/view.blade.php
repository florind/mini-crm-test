@extends('admin.layout', ['page_title' => $company->name, 'page_description' => 'Company details'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    @php
                        $logo = null === $company->logo ? "https://via.placeholder.com/100x80/FAFAFA/E4E4E4?text=logo" : Storage::url($company->logo);
                    @endphp
                    <img class="company-logo" src="{{ $logo }}" alt="">
                </div>
                <div class="box-body">

                    <p><strong>Email: </strong>{{ $company->email ?? "-" }}</p>
                    <p><strong>Website: </strong>{{ $company->website ?? "-" }}</p>
                    <div>
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" href="{{ route('companies.edit', ['company' => $company->id]) }}" role="button">
                                <i class="fa fa-fw fa-pencil"></i>
                            </a>
                            <a class="btn btn-default action_view_delete" data-token="{{ @csrf_token() }}" href="{{ route('companies.delete', ['company' => $company->id]) }}" data-redirect="{{ route('companies') }}" role="button">
                                <i class="fa fa-fw fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <h4>Employees</h4>
            @include('admin/employees/employee_table')
        </div>
    </div>
@endsection