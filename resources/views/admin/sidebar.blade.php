<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ \Illuminate\Support\Facades\Request::routeIs('admin') ? 'active' : '' }}"><a href="{{ route('admin') }}"><i class="fa fa-fw fa-dashboard"></i> <span>Home</span></a></li>
            <li class="{{ \Illuminate\Support\Facades\Request::routeIs('companies', 'companies.*') ? 'active' : '' }}"><a href="{{ route('companies') }}"><i class="fa fa-fw fa-building"></i> <span>Companies</span></a></li>
            <li class="{{ \Illuminate\Support\Facades\Request::routeIs('employees', 'employees.*') ? 'active' : '' }}"><a href="{{ route('employees') }}"><i class="fa fa-fw fa-users"></i> <span>Employees</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>