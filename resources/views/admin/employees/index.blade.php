@extends('admin.layout', ['page_title' => 'Employees', 'page_description' => 'Listing'])

@section('content')
   @include('admin/employees/employee_table')
@endsection