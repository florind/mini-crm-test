<div class="form-group required {{ $errors->has('first_name') ? 'has-error' : '' }}">
    {{ Form::label('first_name', 'First Name') }}
    {{ Form::text('first_name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group required {{ $errors->has('last_name') ? 'has-error' : '' }}">
    {{ Form::label('last_name', 'Last Email') }}
    {{ Form::text('last_name', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {{ Form::label('email', 'Email') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    {{ Form::label('phone', 'Phone') }}
    {{ Form::text('phone', null, ['class' => 'form-control']) }}
</div>
<div class="form-group {{ $errors->has('company_id') ? 'has-error' : '' }}">
    {{ Form::label('company_id', 'Company') }}
    {{ Form::select('company_id', $companies, null, ['placeholder' => '-- no company --', 'class' => 'form-control']) }}
</div>

<div class="box-footer">
    {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
</div>
