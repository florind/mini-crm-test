@php
    $name = sprintf("%s %s", $employee->first_name, $employee->last_name);
@endphp

@extends('admin.layout', ['page_title' => $name, 'page_description' => 'Employee details'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <p><strong>Email: </strong>{{ $employee->email ?? "-" }}</p>
                    <p><strong>Phone: </strong>{{ $employee->phone ?? "-" }}</p>
                    <p><strong>Company: </strong>
                        @if(null !== $employee->company)
                            <a href="{{ route("companies.view", ['company' => $employee->company->id]) }}">{{ $employee->company->name }}</a>
                        @else
                            -
                        @endif
                    </p>
                    <div>
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" href="{{ route('employees.edit', ['employee' => $employee->id]) }}" role="button">
                                <i class="fa fa-fw fa-pencil"></i>
                            </a>
                            <a class="btn btn-default action_view_delete" data-token="{{ @csrf_token() }}" href="{{ route('employees.delete', ['employee' => $employee->id]) }}" data-redirect="{{ route('employees') }}" role="button">
                                <i class="fa fa-fw fa-trash"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection