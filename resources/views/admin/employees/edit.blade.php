@php
    $name = sprintf("%s %s", $employee->first_name, $employee->last_name);
@endphp

@extends('admin.layout', ['page_title' => $name, 'page_description' => 'Edit details'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{ Form::model($employee, ['route' => ['employees.update', $employee->id]]) }}
                        @include('admin/employees/form_fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection