{{ $employees->links() }}
<div class="box box-primary">
    <div class="box-header with-border">
        <a href="{{ route('employees.create') }}" class="btn btn-success">create employee</a>
    </div>
    <div class="box-body">
        <table class="table table-bordered table-striped table-condensed datatable">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            @if($employees->count() === 0)
                <tr>
                    <td colspan="5" align="center"><span class="text-muted">No employees</span></td>
                </tr>
            @endif
            @foreach($employees as $employee)
                <tr>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->email ?? "-" }}</td>
                    <td>{{ $employee->phone ?? "-" }}</td>
                    <td>
                        @if(null !== $employee->company)
                            <a href="{{ route('companies.view', ['company' => $employee->company->id]) }}">{{ $employee->company->name }}</a>
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        <div class="btn-group" role="group">
                            <a class="btn btn-default" href="{{ route('employees.view', ['employee' => $employee->id]) }}" role="button">
                                <i class="fa fa-fw fa-eye"></i>
                            </a>
                            <a class="btn btn-default" href="{{ route('employees.edit', ['employee' => $employee->id]) }}" role="button">
                                <i class="fa fa-fw fa-pencil"></i>
                            </a>
                            <a class="btn btn-default action_list_delete" data-token="{{ @csrf_token() }}" href="{{ route('employees.delete', ['employee' => $employee->id]) }}" role="button">
                                <i class="fa fa-fw fa-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
{{ $employees->links() }}