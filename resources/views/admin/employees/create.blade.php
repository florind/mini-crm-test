@extends('admin.layout', ['page_title' => 'Employee', 'page_description' => 'Create an employee'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {{ Form::open(['route' => 'employees.store']) }}
                        @include('admin/employees/form_fields')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection