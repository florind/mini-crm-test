@extends('admin.layout', ['page_title' => 'Dashboard', 'page_description' => 'The most useless dashboard'])

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-fw fa-building"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Companies</span>
                    <span class="info-box-number">{{ $companies }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>


        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Employees</span>
                    <span class="info-box-number">{{ $employees }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
    </div>
@endsection