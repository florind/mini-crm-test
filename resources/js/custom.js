(function($) {
    $(function() {
        $('a.action_list_delete').on('click', function () {
            let button = $(this);
            let url = button.attr('href');
            let token = button.data('token');

            deleteResource(url, token, function () {
                button.closest('tr').remove();
            }.bind(this));

            return false;
        });

        $('a.action_view_delete').on('click', function () {
            let button = $(this);
            let url = button.attr('href');
            let redirect = button.data('redirect');
            let token = button.data('token');

            deleteResource(url, token, function () {
                window.location.href = redirect;
            }.bind(this));

            return false;
        });

        function confirmDelete(callback) {
            let $modal = $('#confirm-delete-modal');

            $modal.find('button.confirm').off('click').on('click', function () {
                callback();
                $modal.modal('hide');
            });

            $modal.modal('show');
        }

        function deleteResource(url, token, callback) {
            confirmDelete(function () {
                $.ajax({
                    method: 'DELETE',
                    url: url,
                    data: {_token: token}
                }).done(function () {
                    callback();
                }).fail(function () {
                    $('#delete-failed-modal').modal('show');
                });
            }.bind(this));
        }
    });
})(jQuery);


