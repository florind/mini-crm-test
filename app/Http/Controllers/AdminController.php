<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/index', ['employees' => Employee::count(), 'companies' => Company::count()]);
    }
}
