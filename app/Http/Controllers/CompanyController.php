<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    private $fieldConstraints = [
        'name'    => 'required',
        'email'   => 'nullable|email',
        'website' => 'nullable|url',
        'logo'    => 'nullable|image|dimensions:min_width=100,min_height=100',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return view('admin/companies/index', [ 'companies' => Company::paginate(10) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
	    return view('admin/companies/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $data    = $this->validate($request, $this->fieldConstraints);

        $company->fill($data);

        if (array_key_exists('logo', $data)) {
            $company->logo = $data['logo']->store('/', 'public');
        }

        $company->save();

        return redirect()->route('companies.view', ['company' => $company->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view(
            '/admin/companies/view',
            [
                'company' => $company,
                'employees' => $company->employees()->paginate(10)
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('/admin/companies/edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $data = $this->validate($request, $this->fieldConstraints);
        $company->fill($data);

        if (array_key_exists('logo', $data)) {
            $company->logo = $data['logo']->store('/', 'public');
        }

        $company->save();

        return redirect()->route('companies.view', ['company' => $company->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return response()->json();
    }
}
