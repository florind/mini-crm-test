<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::redirect('/', '/admin');
Route::redirect('/register', '/');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::get('/', 'AdminController@index')->name('admin');

	Route::group(['prefix' => 'companies'], function() {
		Route::get('/', 'CompanyController@index')->name('companies');
        Route::get('/create', 'CompanyController@create')->name('companies.create');
        Route::post('/store', 'CompanyController@store')->name('companies.store');
        Route::get('/{company}', 'CompanyController@show')->name('companies.view');
		Route::get('/{company}/edit', 'CompanyController@edit')->name('companies.edit');
		Route::post('/{company}/update', 'CompanyController@update')->name('companies.update');
		Route::delete('/{company}/delete', 'CompanyController@destroy')->name('companies.delete');
	});

	Route::group(['prefix' => 'employees'], function() {
		Route::get('/', 'EmployeeController@index')->name('employees');
        Route::get('/create', 'EmployeeController@create')->name('employees.create');
        Route::post('/store', 'EmployeeController@store')->name('employees.store');
        Route::get('/{employee}', 'EmployeeController@show')->name('employees.view');
		Route::get('/{employee}/edit', 'EmployeeController@edit')->name('employees.edit');
		Route::post('/{employee}/update', 'EmployeeController@update')->name('employees.update');
		Route::delete('/{employee}/delete', 'EmployeeController@destroy')->name('employees.delete');
	});
});

Route::get('/home', 'HomeController@index')->name('home');
